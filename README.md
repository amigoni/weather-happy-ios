# What is it? #
A simple weather app for iOS that uses expressive icons to give a quick idea of the weather over the next 5 days.

# Features: #
- Openweathermap.org API for weather forecast
- Google Maps API to search and get places coordinates
- Save places or use current location