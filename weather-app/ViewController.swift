//
//  ViewController.swift
//  weather-app
//
//  Created by Leonardo Amigoni on 11/4/15.
//  Copyright © 2015 Mozzarello. All rights reserved.
//

import UIKit
import CoreLocation
import SwiftyJSON
import SwiftDate

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, CLLocationManagerDelegate {

    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    var notificationCenter = NSNotificationCenter.defaultCenter()
    var firstLoaded = false
    var fiveDayWeatherArray = Dictionary<Int,Array<Weather3Hour>>()
    var fiveSortedSections = [Int]()
    
    var locationManager = CLLocationManager()
    var locationDevice: Location!
    var locationTrialCount = 0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        
        if Dataservice.ds.currentLocation != nil {
            cityLabel.text = "Just a second"
            timeLabel.text = "Loading..."
        }
        
        if (CLLocationManager.locationServicesEnabled()){
            self.locationManager.delegate = self
            self.locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            self.locationManager.requestWhenInUseAuthorization()
            if Dataservice.ds.currentLocation == nil {
               self.locationManager.requestLocation()
            }
        }
        
        //Setup Listeners
        self.notificationCenter.addObserver(self, selector: "updateFiveDays:", name: "gotFiveDays", object: nil)
        
        self.notificationCenter.addObserver(self, selector: "changeToLoading", name: "resetWeatherView", object: nil)
        
        //Initialize persistant data
        if let units = NSUserDefaults.standardUserDefaults().valueForKey("units") as? String {
            Dataservice.ds.units = units
        } else {
            Dataservice.ds.units = "imperial"
        }
        
        if let currentLocationData = NSUserDefaults.standardUserDefaults().objectForKey("currentLocation") as? NSData {
            
            Dataservice.ds.currentLocation =  NSKeyedUnarchiver.unarchiveObjectWithData(currentLocationData) as! Location
            Dataservice.ds.getWeather(Dataservice.ds.currentLocation)
        }
        
        if let savedLocations = NSUserDefaults.standardUserDefaults().objectForKey("savedLocations") as? NSData {
            Dataservice.ds.savedLocations = NSKeyedUnarchiver.unarchiveObjectWithData(savedLocations) as! [Location]
        }
    }
    
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]){
        if let location = locations.first {
            print("Got Location: \(location.coordinate.latitude)")
            self.locationDevice = Location(name: "Current Location", latitude: location.coordinate.latitude as Double, longitude: location.coordinate.longitude as Double)
            
            print("Getting Current Location Weather")
            Dataservice.ds.getWeather(self.locationDevice)
            
        } else {
            self.alertLocationFailure()
        }
        locationTrialCount = 0
    }
    
    
    func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
        print("Failed to find user's location: \(error.localizedDescription)")
        if locationTrialCount <= 3 {
            locationTrialCount++
            self.locationManager.requestLocation()
        } else {
            //Alert that location doesn't work
            self.alertLocationFailure()
            locationTrialCount = 0
        }
    }
    
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return fiveSortedSections.count
    }
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fiveDayWeatherArray[fiveSortedSections[section]]!.count
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCellWithIdentifier("ThreeHourTableViewCell") as? ThreeHourTableViewCell {
            
            var section = fiveDayWeatherArray[fiveSortedSections[indexPath.section]]
            let weather = section![indexPath.row]
            cell.configure(weather)
            return cell
        } else {
           return ThreeHourTableViewCell()
        }
    }
    
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if let cell = tableView.dequeueReusableCellWithIdentifier("HeaderTableViewCell") as? HeaderTableViewCell {
            let date = fiveDayWeatherArray[fiveSortedSections[section]]![0].time
            cell.configure(date)
            
            return cell
        } else {
            return HeaderTableViewCell()
        }
    }
    
    
    func updateFiveDays(data: NSNotification){
        fiveDayWeatherArray = Dataservice.ds.fiveDayHourlyForecast
        fiveSortedSections = Dataservice.ds.sortedFiveDay
        cityLabel.text = Dataservice.ds.cityName
        timeLabel.text = "5 Day Forecast"
        tableView.reloadData()
    }
    
    
    func changeToLoading(){
        cityLabel.text = "Just a second"
        timeLabel.text = "Loading..."
        fiveDayWeatherArray = Dictionary<Int,Array<Weather3Hour>>()
        fiveSortedSections = [Int]()
        tableView.reloadData()
    }
    
    
    func alertLocationFailure(){
        let alert = UIAlertController(title: "Can't get your Location", message: "Sorry we can't get your current Location.\nTry again later or try searching a city.", preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    
    @IBAction func onCurrentLocationButtonPressed(sender: AnyObject) {
        changeToLoading()
        self.locationManager.requestLocation()
    }
    
}
