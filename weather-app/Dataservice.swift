//
//  Dataservice.swift
//  weather-app
//
//  Created by Leonardo Amigoni on 11/12/15.
//  Copyright © 2015 Mozzarello. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import SwiftDate
import GoogleMaps

let URL_BASE = "http://api.openweathermap.org/data/2.5/"
let FIVE_DAY_FORECAST = "forecast?"
let DAILY_FORECAST = "forecast/daily?"
let APPID = "&APPID=9db8c6bbfe59ecc181054fd1f8a3cbcb"
let UNITS_METRIC = "&units=metric"
let UNITS_IMPERIAL = "&units=imperial"


class Dataservice {
    static let ds = Dataservice()
    
    var cityName: String!
    var units = "imperial"
    var fiveDayHourlyForecast = Dictionary<Int, Array<Weather3Hour>>()
    var sortedFiveDay = [Int]()
    var notificationCenter = NSNotificationCenter.defaultCenter()
    var placesClient: GMSPlacesClient?
    var autoCompletePlaces = [GMSAutocompletePrediction]()
    var currentLocation: Location!
    var savedLocations = [Location]()
    let secondsFromGMT = Double(NSTimeZone.localTimeZone().secondsFromGMT)
    
    
    func getWeather(location: Location){
        self.notificationCenter.postNotificationName("resetWeatherView", object: nil, userInfo: nil)
        self.currentLocation = location
        self.saveCurrentLocation()
        
        let coordinates = "lat=\(location.latitude)&lon=\(location.longitude)"
        
        //Get 5 day hourly forecast
        let hourlyURLString = "\(URL_BASE)\(FIVE_DAY_FORECAST)\(coordinates)\(APPID)"
        
        Alamofire.request(.GET, hourlyURLString).responseJSON{response in
            if let json = response.result.value {
                print("GOT 5 DAY HOURLY FORECAST FOR \(location.name)")
                self.fiveDayHourlyForecast = Dictionary<Int, Array<Weather3Hour>>()
                self.sortedFiveDay = [Int]()
                
                var data = JSON(json)
                self.cityName = location.name
                var list = data["list"]
                
                for var x = 0; x < list.count; x++ {
                    let item = list[x]
                    let timeNS = NSDate(timeIntervalSince1970: (item["dt"].doubleValue + self.secondsFromGMT))
                    let timeHour = timeNS.hour
                    let weatherID = self.parseWeatherIcon(Int(item["weather"][0]["id"].stringValue)!, hour: timeHour)
                    let weatherDescription = item["weather"][0]["description"].stringValue
                    let windSpeed = Int(round(item["wind"]["speed"].doubleValue))
                    let temp = item["main"]["temp"].doubleValue
                    let humidity = item["main"]["humidity"].stringValue
                    
                    let weatherData = Weather3Hour(time: timeNS, weatherID: weatherID , weatherDescription: weatherDescription, windSpeed: "\(windSpeed)", temp: temp, humidity: humidity)
                
                    if self.fiveDayHourlyForecast[timeNS.day] == nil {
                        self.fiveDayHourlyForecast[timeNS.day] = [Weather3Hour]()
                        self.fiveDayHourlyForecast[timeNS.day]!.append(weatherData)
                    } else {
                        self.fiveDayHourlyForecast[timeNS.day]!.append(weatherData)
                    }
                }
                
                self.sortedFiveDay = self.fiveDayHourlyForecast.keys.sort(<)
                
                self.notificationCenter.postNotificationName("gotFiveDays", object: nil, userInfo: nil)
            } else {
                print("Error getting Weather: \(response.debugDescription)")
                self.alertNoConnection()
            }
        }
    }
    
    
    func parseWeatherIcon (weatherId: Int, hour: Int) -> String {
        var icon = ""
        
        if weatherId >= 200 && weatherId <= 299 {
            //Thunder
            icon = "ThunderstormMany"
        } else if weatherId == 300 || weatherId == 301 || weatherId == 310 || weatherId == 301 {
            //Drizzle One Drop
            icon = "CloudOneRainDrop"
        } else if weatherId == 311 || weatherId == 313 || weatherId == 321 {
            //Drizzle Two Drop
            icon = "CloudTwoRainDrops"
        } else if weatherId == 302 || weatherId == 312 || weatherId == 314 {
            //Drizzle Heavy Many Drop
            icon = "CloudManyRainDrops"
        } else if weatherId == 500 || weatherId == 501  {
            //Drizzle One Drop
            icon = "CloudOneRainDrop"
        } else if  weatherId == 502 || weatherId == 503 || weatherId == 520 || weatherId == 521 {
            //Rain One Cloud
            icon = "CloudHeavyRain"
        } else if weatherId == 504 || weatherId == 522 || weatherId == 531 {
            //Rain Two Cloud
            icon = "CloudsHeavyRain"
        } else if weatherId >= 600 && weatherId <= 699 {
            //Snow
            icon = "CloudSnow"
        } else if weatherId == 701 || weatherId == 711 || weatherId == 721 || weatherId == 731 || weatherId == 741 || weatherId == 751 || weatherId == 761 || weatherId == 762 || weatherId == 771 {
            //Fog
            icon = "CloudFog"
        } else if weatherId == 781 {
            //Tornado
            icon = "Tornado"
        } else if weatherId == 800 {
            //Sunny
            icon = "Sun"
            if (hour < 6 || hour > 18) {
                icon = "MoonHalf"
            }
        } else if weatherId == 801 {
            //Sun & Cloud
            icon = "CloudSun"
            if (hour < 6 || hour > 18) {
                icon = "MoonHalfCloudLight"
            }
        } else if weatherId == 802 {
            //Sun & Clouds
            icon = "CloudsSun"
            if (hour < 6 || hour > 18) {
                icon = "MoonHalfCloudDark"
            }
        } else if weatherId == 803 {
            //One Cloud
            icon = "CloudLightSmile"
        } else if weatherId == 804 {
            //One Cloud
            icon = "CloudsLightDark"
        } else if weatherId == 900{
            //Tornado
            icon = "Tornado"
        } else if weatherId == 901{
            //Tropical Storm
            icon = "ThunderstormMany"
        } else if weatherId == 902{
            //Hurricane
            icon = "Huricane"
        } else if weatherId == 903{
            //Tornado
            icon = "ThermometerCold"
        } else if weatherId == 904{
            //Tornado
            icon = "ThermometerHot"
        } else if weatherId == 905{
            //WindWarning
            icon = "WindWarning"
        } else if weatherId == 903{
            //Hail/CloudSnow
            icon = "CloudSnow"
        } else if weatherId == 951 || weatherId == 952 || weatherId == 953 || weatherId == 954{
            //Gentle Breeze
            icon = "WindLittle"
        } else if weatherId == 955 || weatherId == 956 || weatherId == 957 {
            //Moderate Breeze
            icon = "WindSymbol"
        } else if weatherId == 958 || weatherId == 959{
            //Strong Breeze
            icon = "WindWarning"
        } else if weatherId == 960 || weatherId == 961{
            //Storm
            icon = "ThunderstormWarning"
        } else if weatherId == 962 {
            //Hurricane
            icon = "Huricane"
        }
    
        return icon
    }
    
    
    func getLocations(text: String) {
        self.autoCompletePlaces = [GMSAutocompletePrediction]()
        let filter = GMSAutocompleteFilter()
        filter.type = GMSPlacesAutocompleteTypeFilter.City
        
        placesClient?.autocompleteQuery(text, bounds: nil, filter: filter, callback: { (results, error: NSError?) -> Void in
            
            if let error = error {
                print("Autocomplete error \(error)")
                self.alertNoConnection()
            }
            
            for result in results! {
                if let result = result as? GMSAutocompletePrediction {
                    print(result.attributedFullText.string)
                    self.autoCompletePlaces.append(result)
                }
            }
            
            self.notificationCenter.postNotificationName("gotLocations", object: nil, userInfo: nil)
        })
    }
    
    
    func saveCurrentLocation(){
        let locationData = NSKeyedArchiver.archivedDataWithRootObject(self.currentLocation)
        NSUserDefaults.standardUserDefaults().setObject(locationData, forKey: "currentLocation")
        NSUserDefaults.standardUserDefaults().synchronize()
        print("Saved Current Location: \(self.currentLocation.name)")
    }
    
    
    func saveLocations(){
        self.savedLocations.sortInPlace {$0.name < $1.name}
        
        let savedLocationsData = NSKeyedArchiver.archivedDataWithRootObject(self.savedLocations)
        NSUserDefaults.standardUserDefaults().setObject(savedLocationsData, forKey: "savedLocations")
        NSUserDefaults.standardUserDefaults().synchronize()
        print("Saved Locations")
    }
    
    
    func addNewLocation(placeID: String){
        placesClient!.lookUpPlaceID(placeID, callback: { (place: GMSPlace?, error: NSError?) -> Void in
            if let error = error {
                print("Error looking up place id: \(error.localizedDescription)")
                self.alertNoConnection()
            }
            
            if let place = place {
                print("Got Location Detail for: \(place.name)")
                
                self.currentLocation = Location(name: place.name, latitude: place.coordinate.latitude as Double, longitude: place.coordinate.longitude as Double)
                self.saveCurrentLocation()
                self.savedLocations.append(self.currentLocation)
                self.saveLocations()
                
                self.notificationCenter.postNotificationName("gotLocationDetail", object: nil, userInfo: nil)
            } else {
                print("No place details for \(placeID)")
            }
        })
    }
    
    
    func changeUnits(newUnit: String){
        if newUnit == "imperial" {
            units = "imperial"
            NSUserDefaults.standardUserDefaults().setValue("imperial", forKey: "units")
        } else if newUnit == "metric" {
            units = "metric"
            NSUserDefaults.standardUserDefaults().setValue("metric", forKey: "units")
        }
        
        NSUserDefaults.standardUserDefaults().synchronize()
    }
    
    
    func alertNoConnection(){
        let alert = UIAlertController(title: "Error Connecting", message: "Sorry we can't seem to connect to the internet.\nTry again later.", preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
        UIApplication.sharedApplication().delegate!.window!!.rootViewController?.presentViewController(alert, animated: true, completion: nil)
    }
    
}