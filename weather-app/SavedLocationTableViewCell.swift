//
//  SavedLocationTableViewCell.swift
//  weather-app
//
//  Created by Leonardo Amigoni on 11/16/15.
//  Copyright © 2015 Mozzarello. All rights reserved.
//

import UIKit
import GoogleMaps

class SavedLocationTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    var data: Location!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func configure(data: Location){
        self.data = data
        titleLabel.text = data.name
    }
    
}
