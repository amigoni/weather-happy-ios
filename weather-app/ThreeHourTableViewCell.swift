//
//  ThreeHourTableViewCell.swift
//  weather-app
//
//  Created by Leonardo Amigoni on 11/12/15.
//  Copyright © 2015 Mozzarello. All rights reserved.
//

import UIKit

class ThreeHourTableViewCell: UITableViewCell {
    @IBOutlet weak var hourNumberLabel: UILabel!
    @IBOutlet weak var hourAMPMLabel: UILabel!
    @IBOutlet weak var weatherIcon: UIImageView!
    @IBOutlet weak var weatherDescriptionLabel: UILabel!
    @IBOutlet weak var tempLabel: UILabel!
    @IBOutlet weak var humidityLabel: UILabel!
    @IBOutlet weak var windSpeedLabel: UILabel!
    @IBOutlet weak var windUnitsLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configure(weatherData: Weather3Hour){
        convertHour(weatherData.time.hour)
        weatherIcon.image = UIImage(named: weatherData.weatherId)
        weatherDescriptionLabel.text = weatherData.weatherDescription.capitalizedString
        tempLabel.text = "\(convertTemperature(weatherData.temp, unit: Dataservice.ds.units))˚"
        humidityLabel.text = "\(weatherData.humidity)%"
        windSpeedLabel.text = weatherData.windSpeed
        
        if Dataservice.ds.units == "metric" {
            windUnitsLabel.text = "kph"
        } else if Dataservice.ds.units == "imperial" {
            windUnitsLabel.text = "mph"
        }
        
        self.layer.backgroundColor = UIColor(white: 0.1, alpha: 1).CGColor
    }
    
    
    func convertTemperature(tempIn: Double, unit: String) -> Int{
        var tempOut = 0
        
        if unit == "metric" {
            tempOut = Int(round(tempIn - 273.15))
        } else if unit == "imperial" {
            let conversion = (tempIn-273.15) * 1.8 + 32.00
            tempOut = Int(round(conversion))
        }
        
        return tempOut
    }
    
    func convertHour(hour: Int){
        if Dataservice.ds.units == "imperial" {
            var newTime = hour
            if newTime == 0 {
                hourAMPMLabel.text = "AM"
                newTime = 12
            } else if(newTime > 0 && newTime < 12){
                hourAMPMLabel.text = "AM"
            }else if (newTime == 12){
                hourAMPMLabel.text = "PM"
            }
            else if (newTime > 12){
                newTime -= 12
                hourAMPMLabel.text = "PM"
            }
            hourNumberLabel.text = "\(newTime)"
        } else if Dataservice.ds.units == "metric" {
            hourAMPMLabel.text = ""
            if hour < 10 {
                hourNumberLabel.text = "0\(hour):00"
            } else {
                hourNumberLabel.text = "\(hour):00"
            }
        }
    }
    
}
