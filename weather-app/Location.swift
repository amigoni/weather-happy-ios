//
//  Location.swift
//  weather-app
//
//  Created by Leonardo Amigoni on 11/16/15.
//  Copyright © 2015 Mozzarello. All rights reserved.
//

import Foundation

class Location: NSObject, NSCoding {
    var name: String!
    var latitude: Double!
    var longitude: Double!
    
    init(name: String, latitude: Double, longitude: Double){
        self.name = name
        self.latitude = latitude
        self.longitude = longitude
    }
    
    override init() {
        
    }
    
    required convenience init?(coder aDecoder: NSCoder){
        self.init()
        self.name = aDecoder.decodeObjectForKey("name") as? String
        self.latitude = aDecoder.decodeObjectForKey("latitude") as? Double
        self.longitude = aDecoder.decodeObjectForKey("longitude") as? Double
    }
    
    func encodeWithCoder(aCoder: NSCoder) {
        aCoder.encodeObject(self.name, forKey: "name")
        aCoder.encodeObject(self.latitude, forKey: "latitude")
        aCoder.encodeObject(self.longitude, forKey: "longitude")
    }
    
}