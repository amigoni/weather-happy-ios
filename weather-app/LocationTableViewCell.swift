//
//  LocationTableViewCell.swift
//  weather-app
//
//  Created by Leonardo Amigoni on 11/15/15.
//  Copyright © 2015 Mozzarello. All rights reserved.
//

import UIKit
import GoogleMaps

class LocationTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    var data: GMSAutocompletePrediction!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func configure(data: GMSAutocompletePrediction, title: String){
        self.data = data
        titleLabel.text = title
    }
    
}
