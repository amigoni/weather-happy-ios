//
//  LocationViewController.swift
//  weather-app
//
//  Created by Leonardo Amigoni on 11/15/15.
//  Copyright © 2015 Mozzarello. All rights reserved.
//

import UIKit
import GoogleMaps

class LocationViewController: UIViewController, UITableViewDataSource, UITableViewDelegate,UISearchBarDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var temperatureButton: UIButton!
    
    var inSearchMode = false
    var savedLocations = [Location]()
    var autoCompleteLocations = [GMSAutocompletePrediction]()
    var notificationCenter = NSNotificationCenter.defaultCenter()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchBar.delegate = self
        searchBar.returnKeyType = UIReturnKeyType.Done
        tableView.delegate = self
        tableView.dataSource = self
        
        Dataservice.ds.placesClient = GMSPlacesClient()
        
        savedLocations = Dataservice.ds.savedLocations
        
        if Dataservice.ds.units == "imperial" {
            temperatureButton.setTitle("F˚", forState: UIControlState.Normal)
        } else if Dataservice.ds.units == "metric" {
            temperatureButton.setTitle("C˚", forState: UIControlState.Normal)
        }
        
        let textFieldInsideSearchBar = searchBar.valueForKey("searchField") as? UITextField
        textFieldInsideSearchBar?.textColor = UIColor.whiteColor()
        
        self.notificationCenter.addObserver(self, selector: "updateAutocompleteLocations", name: "gotLocations", object: nil)
        
        self.notificationCenter.addObserver(self, selector: "updateLocationDetail", name: "gotLocationDetail", object: nil)
    }
    
    
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return UIStatusBarStyle.LightContent
    }

    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if inSearchMode == true {
            return self.autoCompleteLocations.count
        } else {
            return self.savedLocations.count
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if inSearchMode == true {
            if let cell = tableView.dequeueReusableCellWithIdentifier("LocationTableViewCell") as? LocationTableViewCell {
                let title = self.autoCompleteLocations[indexPath.row].attributedFullText.string
                cell.configure(self.autoCompleteLocations[indexPath.row], title: title)
                return cell
            } else {
                return LocationTableViewCell()
            }
        } else {
            if let cell = tableView.dequeueReusableCellWithIdentifier("SavedLocationTableViewCell") as? SavedLocationTableViewCell {
                let location = self.savedLocations[indexPath.row]
                cell.configure(location)
                return cell
            } else {
               return SavedLocationTableViewCell()
            }
        }
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if inSearchMode == true {
            let location = autoCompleteLocations[indexPath.row]
            Dataservice.ds.addNewLocation(location.placeID)
        } else {
            let location = savedLocations[indexPath.row]
            Dataservice.ds.getWeather(location)
            dismissViewControllerAnimated(true, completion: nil)
        }
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == UITableViewCellEditingStyle.Delete {
            savedLocations.removeAtIndex(indexPath.row)
            Dataservice.ds.savedLocations = savedLocations
            Dataservice.ds.saveLocations()
            
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Automatic)
        }
    }
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar.text == nil || searchBar.text == "" {
            inSearchMode = false
            view.endEditing(true)
        } else {
            inSearchMode = true
            Dataservice.ds.getLocations(searchText)
        }
    }
    
    func updateAutocompleteLocations(){
        self.autoCompleteLocations = Dataservice.ds.autoCompletePlaces
        tableView.reloadData()
    }
    
    func updateLocationDetail(){
        Dataservice.ds.getWeather(Dataservice.ds.currentLocation)
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func onDonePressed(sender: UIButton) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func onTempButtonPressed(sender: UIButton) {
        if Dataservice.ds.units == "metric" {
            temperatureButton.setTitle("F˚", forState: UIControlState.Normal)
            Dataservice.ds.changeUnits("imperial")
        } else if Dataservice.ds.units == "imperial" {
            temperatureButton.setTitle("C˚", forState: UIControlState.Normal)
            Dataservice.ds.changeUnits("metric")
        }
        
        if Dataservice.ds.currentLocation != nil {
            Dataservice.ds.getWeather(Dataservice.ds.currentLocation)
        }
        dismissViewControllerAnimated(true, completion: nil)
    }

}
