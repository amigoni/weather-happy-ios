//
//  Weather3Hour.swift
//  weather-app
//
//  Created by Leonardo Amigoni on 11/12/15.
//  Copyright © 2015 Mozzarello. All rights reserved.
//

import Foundation

class Weather3Hour {
    var _time: NSDate
    var _temp: Double
    //var _tempMin: String
    //var _tempMax: String
    var _humidity: String
    var _weatherId: String
    var _weatherDescription: String
    var _windSpeed: String
    
    var time: NSDate {
        return _time
    }
    
    var temp: Double {
        return _temp
    }
    
    var humidity: String {
        return _humidity
    }
    
    var weatherId: String {
        return _weatherId
    }
    
    var weatherDescription: String {
        return _weatherDescription
    }
    
    var windSpeed: String {
        return _windSpeed
    }
    
    init(time: NSDate, weatherID: String, weatherDescription: String, windSpeed: String, temp: Double, humidity: String){
        _time = time
        _weatherId = weatherID
        _weatherDescription = weatherDescription
        _windSpeed = windSpeed
        _temp = temp
        _humidity = humidity
    }
}