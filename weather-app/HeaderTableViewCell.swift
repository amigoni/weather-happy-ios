//
//  HeaderTableViewCell.swift
//  weather-app
//
//  Created by Leonardo Amigoni on 11/14/15.
//  Copyright © 2015 Mozzarello. All rights reserved.
//

import UIKit

class HeaderTableViewCell: UITableViewCell {
    @IBOutlet weak var dateLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configure(date: NSDate){
        var weekDay = "Sun"
        let wd = date.weekday
        
        if(wd == 1) {
            weekDay = "Sun"
        } else if (wd == 2){
            weekDay = "Mon"
        } else if (wd == 3){
            weekDay = "Tue"
        } else if (wd == 4){
            weekDay = "Wed"
        } else if (wd == 5){
            weekDay = "Thu"
        } else if (wd == 6){
            weekDay = "Fri"
        } else if (wd == 7){
            weekDay = "Sat"
        }
        
        let monthName = date.monthName.capitalizedString
        let dayStr = "\(date.day)"
       
        var numberLetter = "th"
        if date.day == 1 { 
            numberLetter = "st"
        }
        else if date.day == 2 {
            numberLetter = "nd"
        }
        else if date.day == 3 {
            numberLetter = "rd"
        }
        
        dateLabel.text = "\(weekDay) \(monthName) \(dayStr)\(numberLetter)"
    }

}
